# Neovim configuration

## Description
This is my neovim configuration.

## Installation
my neovim setup is automaticaly import with specials scripts,
if you want to see how you can do it by yourself, refers to : [Nvim portable configuration](https://github.com/plagache/nvim_fmr)

## Support

[The neovim repository](https://github.com/neovim/neovim)
[Starting point for a new neovim configuration](https://github.com/nvim-lua/kickstart.nvim)

## Roadmap
- [x] [Integrated with Nvim FMR for portable configuration](https://github.com/plagache/nvim_fmr)
- [x] Add compartmentalization of plugins, with return. Will allow plugins to failed individually
- [x] Add [grug-far](https://github.com/MagicDuck/grug-far.nvim)
- [ ] look into [lazydev](https://github.com/folke/lazydev.nvim?tab=readme-ov-file)
- [ ] Replace fzy with fzf ?

## Contributing
This repo is public, but I am not open to contributions.

## License
This work is under the intellectual property of the user: plgch
