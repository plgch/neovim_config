-- Modify go to definition for Help files only
vim.api.nvim_create_autocmd({ "FileType" }, {
    pattern = { "help" },
    callback = function(opts)
        vim.keymap.set("n", "gd", "<C-]>", { silent = true, buffer = opts.buf })
    end,
})
