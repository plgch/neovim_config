local options = {

	-- nocompatible = true,                    -- act like a true vim
	-- ttymouse = "xterm2",
	-- nofixendofline = true,              -- disable automatic newline of EOF
	termguicolors = true,

	mouse = "a", -- allow the mouse to be used in neovim

	-- number
	number = true, -- set numbered lines
	relativenumber = true, -- set relative numbered lines

	syntax = "on",

	tabstop = 4, -- insert 2 spaces for a tab
	softtabstop = 4, -- insert 2 spaces for a tab
	shiftwidth = 4, -- the number of spaces inserted for each indentation
	expandtab = true, -- convert tabs to spaces

	list = true, -- display invisible char at end of line

	path = ".,./**",
	wildmenu = true,
	wildmode = { "longest", "full" },
	showcmd = true,
	ignorecase = true, -- ignore case in search patterns
	hlsearch = true, -- highlight all matches on previous search pattern
	incsearch = true,
	smartcase = true, -- smart case
	smartindent = true, -- make indenting smarter again
	hidden = true,
	wrap = false, -- display lines as one long line
	showmode = true, -- show current mode like -- INSERT --
	showtabline = 1, -- show when active
	winbar = "%f %m", -- window bar options displayed
	-- tabline = "%t %m",                          -- tab bar options displayed

	scrolloff = 8, -- is one of my fav
	sidescrolloff = 8,

	swapfile = false, -- creates a swapfile

	cmdheight = 1, -- more space in the neovim command line for displaying messages

	completeopt = { "menuone", "noinsert", "noselect" }, -- mostly just for cmp
	fileencoding = "utf-8", -- the encoding written to a file

	conceallevel = 0, -- so that `` is visible in markdown files

	clipboard = "unnamedplus", -- allows neovim to access the system clipboard

	laststatus = 3, -- one global status line to rule them all ]]

	splitbelow = true, -- force all horizontal splits to go below current window
	splitright = true, -- force all vertical splits to go to the right of current window
	timeoutlen = 420, -- time to wait for a mapped sequence to complete (in milliseconds)
	undofile = true, -- enable persistent undo
	updatetime = 420, -- faster completion (4000ms default)
	numberwidth = 4, -- set number column width to 2 {default 4}

	confirm = true, -- We need verbal confirmation to save or not when we close neovim

    inccommand = "split", -- Live preview of command search & replace / substitute
}

vim.opt.shortmess:append("c")
--[[ vim.opt.path:append "**" ]]

for k, v in pairs(options) do
	vim.opt[k] = v
end

vim.cmd("set whichwrap+=<,>,[,],h,l")
vim.cmd([[set iskeyword+=-]])
vim.cmd([[set formatoptions-=cro]]) -- TODO: this doesn't seem to work
