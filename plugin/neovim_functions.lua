-- function prequire(required_name)
--     local pcall_status, instance_required = pcall(require, required_name)
--     if not pcall_status then
--         vim.notify("Failed to load " .. required_name, "error")
--         -- print("error loading : ", required_name)
--     end
--     return pcall_status, instance_required
-- end

function set_keymap(...)
	vim.keymap.set(...)
end

function unset_keymap(...)
	vim.keymap.del(...)
end

function vim.getVisualSelection()
	vim.cmd('noau normal! "vy"')
	local text = vim.fn.getreg("v")
	vim.fn.setreg("v", {})

	text = string.gsub(text, "\n", "")
	if #text > 0 then
		return text
	else
		return ""
	end
end
