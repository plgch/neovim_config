-- Visual options

local options = {
	termguicolors = true,
	background = "dark", -- set background to dark
	cursorline = true, -- highlight the current line
	laststatus = 3, -- set status line to global
}

for k, v in pairs(options) do
	vim.opt[k] = v
end

-- hightline column longer than 80 chars
vim.api.nvim_set_option_value("colorcolumn", "80", {})
