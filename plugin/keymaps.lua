-- Need to make a good strucure for keybind
-- leader c : commands / runners
-- leader d : debugger / breakpoints
-- leader b : buffer
-- leader m : movement / maximize / move to
-- Leader g : go to
-- Leader f : format
-- leader n : nnn / tree
-- Leader t : terminal
-- Leader l : list or LSP
-- Leader s : search
-- Leader v : version
-- Leader r : refactoring
-- Leader h : help / diagnostique / hover

-- LEADER --
vim.g.mapleader = " "
vim.g.maplocalleader = ','

-- OPTIONS --
local opts = { noremap = true, silent = true }

-- MOVEMENT --
-- C-/h/j/k/l to move between buffers
vim.keymap.set("n", "<C-left>", "<cmd>vertical resize -5<CR>", opts)
vim.keymap.set("n", "<C-Down>", "<cmd>resize -5<CR>", opts)
vim.keymap.set("n", "<C-up>", "<cmd>resize +5<CR>", opts)
vim.keymap.set("n", "<C-right>", "<cmd>vertical resize +5<CR>", opts)

-- Moving Visual selection
-- this one is dangerous
-- vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
-- vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Let Cursor to stay at the same place when doing different keymaps
vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- TERMINAL --
vim.keymap.set("t", "<C-h>", "<cmd>wincmd h<CR>", opts)
vim.keymap.set("t", "<C-j>", "<cmd>wincmd j<CR>", opts)
vim.keymap.set("t", "<C-k>", "<cmd>wincmd k<CR>", opts)
vim.keymap.set("t", "<C-l>", "<cmd>wincmd l<CR>", opts)
vim.keymap.set("t", "<Esc>", "<C-\\><C-n>", opts)

-- TABULATION --
vim.keymap.set("n", "<leader>gt", "<cmd>tabe<CR>", { desc = "[G]oto [N]ew [T]ab" }, opts)

-- KEYMAPPING --
vim.keymap.set("n", "<leader>gm", "<cmd>e ~/.config/nvim/plugin/keymaps.lua<cr>", { desc = "[G]oto [M]apping" }, opts)

-- Alternate between buffer --
vim.keymap.set("n", "<leader>bb", "<cmd>e #<cr>", { desc = "[B]uffer from [B]efore" }, opts)
