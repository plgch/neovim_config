--
--
-- ████████╗███████╗██╗     ███████╗███████╗ ██████╗ ██████╗ ██████╗ ███████╗
-- ╚══██╔══╝██╔════╝██║     ██╔════╝██╔════╝██╔════╝██╔═══██╗██╔══██╗██╔════╝
--    ██║   █████╗  ██║     █████╗  ███████╗██║     ██║   ██║██████╔╝█████╗
--    ██║   ██╔══╝  ██║     ██╔══╝  ╚════██║██║     ██║   ██║██╔═══╝ ██╔══╝
--    ██║   ███████╗███████╗███████╗███████║╚██████╗╚██████╔╝██║     ███████╗
--    ╚═╝   ╚══════╝╚══════╝╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝     ╚══════╝
--
--

local builtin = require("telescope.builtin")
-- local extensions = require("telescope").extensions
-- local fb_actions = extensions.file_browser.actions
-- local dap = extensions.dap

local gruvbox = require("gruvbox")
local colors = gruvbox.palette

local TelescopePrompt = {
    -- Prompt
    TelescopePromptNormal = {
        bg = colors.dark1,
    },
    TelescopePromptTitle = {
        fg = colors.dark0,
        bg = colors.neutral_purple,
    },
    TelescopePromptCounter = {
        fg = colors.light1,
    },
    TelescopePromptBorder = {
        bg = colors.dark1,
        fg = colors.dark1,
    },
    TelescopePromptPrefix = {
        fg = colors.neutral_aqua,
    },
    -- Preview
    TelescopePreviewTitle = {
        bg = colors.neutral_green,
        fg = colors.dark0,
    },
    TelescopePreviewBorder = {
        bg = colors.dark0_soft,
        fg = colors.dark0_soft,
    },
    -- Result
    TelescopeResultsTitle = {
        -- fg = colors.dark0,
        -- bg = colors.neutral_yellow,
        -- other option is no title > same colors os the border
        bg = colors.dark0_soft,
        fg = colors.dark0_soft,
    },
    TelescopeResultsBorder = {
        bg = colors.dark0_soft,
        fg = colors.dark0_soft,
    },
    -- Selection
    TelescopeSelectionCaret = {
        bg = colors.dark1,
        fg = colors.neutral_purple,
    },
    TelescopeSelection = {
        bg = colors.dark1,
    },
    -- Matching
    TelescopeMatching = {
        fg = colors.neutral_yellow,
    },
}

for hl, col in pairs(TelescopePrompt) do
    vim.api.nvim_set_hl(0, hl, col)
end

require("telescope").setup({
    defaults = {
        mappings = {
            i = {
                ["<C-r>"] = require("telescope.actions").to_fuzzy_refine,
            },
        },
        vimgrep_arguments = {
            "rg",
            "--color=never",
            "--no-heading",
            "--with-filename",
            "--line-number",
            "--column",
            "--hidden",
            "--smart-case",
        },
        -- prompt_prefix = "> ",
        prompt_prefix = "[~] ",
        selection_strategy = "reset",
        scroll_strategy = "cycle",
        sorting_strategy = "ascending",
        layout_strategy = "horizontal",
        -- layout_strategy = "bottom_pane",
        layout_config = {
            horizontal = {
                mirror = false,
                prompt_position = "top",
                results_height = 1,
                preview_width = 0.50,
                width = { padding = 0 },
                height = { padding = 0 },
                preview_cutoff = 120,
                prompt_title = false,
                results_title = false,
                preview_title = false,
                path_display = true,
            },
            vertical = {
                mirror = false,
                width = { padding = 0 },
            },
        },

        -- Tranparancy of window
        --[[ winblend = 10, ]]
        border = {},
        -- border = false,
        -- borderchars = {
        --     { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
        --     prompt = { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
        --     results = { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
        --     preview = { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
        --     prompt = {"─", "│", " ", "│", '┌', '┐', "│", "│"},
        --     results = {"─", "│", "─", "│", "├", "┤", "┘", "└"},
        --     preview = { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
        -- },
        file_sorter = require("telescope.sorters").get_fzf_sorter,
        file_ignore_patterns = {},
        generic_sorter = require("telescope.sorters").get_fzf_sorter,
        set_env = { ["COLORTERM"] = "truecolor" },                                -- default { }, currently unsupported for shells like cmd.exe / powershell.exe
        file_previewer = require("telescope.previewers").vim_buffer_cat.new,      -- For buffer previewer use `require'telescope.previewers'.vim_buffer_cat.new`
        grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,  -- For buffer previewer use `require'telescope.previewers'.vim_buffer_vimgrep.new`
        qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new, -- For buffer previewer use `require'telescope.previewers'.vim_buffer_qflist.new`

        -- Developer configurations: Not meant for general override
        buffer_previewer_maker = require("telescope.previewers").buffer_previewer_maker,
        cache_picker = {
            num_pickers = -1,
            limit_entries = 1000,
        },
    },
    pickers = {
        current_buffer_fuzzy_find = {
            skip_empty_lines = true,
        },
        -- man_pages = {
        --     sections = {"ALL"}
        -- },
        find_files = {
            hidden = true,
            no_ignore = true,
            no_ignore_parent = true,
        },
        git_files = {
            show_untracked = true,
        },
    },
    extensions = {
        fzf = {
            fuzzy = true,             -- false will only do exact matching
            override_generic_sorter = true, -- override the generic sorter
            override_file_sorter = true, -- override the file sorter
            case_mode = "smart_case", -- or "ignore_case" or "respect_case"
            -- the default case_mode is "smart_case"
        },
        undo = {
            use_delta = true,
            use_custom_command = nil, -- setting this implies `use_delta = false`. Accepted format is: { "bash", "-c", "echo '$DIFF' | delta" }
            side_by_side = false,
            vim_diff_opts = {
                ctxlen = vim.o.scrolloff,
            },
            entry_format = "state #$ID, $STAT, $TIME",
            mappings = {
                i = {
                    -- IMPORTANT: Note that telescope-undo must be available when telescope is configured if
                    -- you want to replicate these defaults and use the following actions. This means
                    -- installing as a dependency of telescope in it's `requirements` and loading this
                    -- extension from there instead of having the separate plugin definition as outlined
                    -- above.
                    ["<cr>"] = require("telescope-undo.actions").yank_additions,
                    ["<S-cr>"] = require("telescope-undo.actions").yank_deletions,
                    ["<C-cr>"] = require("telescope-undo.actions").restore,
                },
            },
        },
        -- media_files = {
        --     -- filetypes whitelist
        --     -- defaults to {"png", "jpg", "mp4", "webm", "pdf"}
        --     filetypes = { "png", "webp", "jpg", "jpeg", "pdf" },
        --     find_cmd = "rg", -- find command (defaults to `fd`)
        -- },
        -- fzy_native = {
        --     override_generic_sorter = true,
        --     override_file_sorter = true,
        -- },
        ["ui-select"] = {
            require("telescope.themes").get_dropdown({
                -- even more opts
            }),
        },
        -- file_browser = {
        --     hidden = true,
        --     initial_mode = "normal",
        --     use_fd = true,
        --     mappings = {
        --         ["i"] = {},
        --         ["n"] = {
        --             -- unmap toggling `fb_actions.toggle_browser`
        --             ["<C-h>"] = fb_actions.toggle_hidden,
        --             h = fb_actions.goto_parent_dir,
        --             ["~"] = fb_actions.goto_home_dir,
        --             f = false,
        --             ["<C-c>"] = require("telescope.actions").close,
        --             q = require("telescope.actions").close,
        --             l = require("telescope.actions").select_default,
        --             e = require("telescope.actions").select_default,
        --         },
        --     },
        -- },
    },
})

require('telescope').load_extension('fzf')
require("telescope").load_extension("undo")
require("telescope").load_extension("ui-select")
-- require("telescope").load_extension("fzy_native")
-- require("telescope").load_extension("dap")
-- require("telescope").load_extension("file_browser")
-- require("telescope").load_extension("media_files")

vim.keymap.set("n", "<leader>sd",
    function() builtin.git_files({ prompt_title = "Dotfiles", cwd = "~/.config/dot_files/" }) end,
    { desc = "[S]earch [D]ot Files" }, opts)

vim.keymap.set("n", "<leader>sa",
    function()
        local path = vim.fn.expand("%:p:h")
        builtin.find_files({ prompt_title = "Angular Components", default_text = vim.fn.expand("%:t:r"), cwd = path })
    end, { desc = "[S]earch [A]ngular Structure" }, opts)

vim.keymap.set("n", "<leader>ss", function() builtin.git_files({ prompt_title = "Script", cwd = "~/base/" }) end,
    { desc = "[S]earch [S]cripts" }, opts)

vim.keymap.set("n", "<leader>sn", function() builtin.git_files({ prompt_title = "Notes", cwd = "~/notes/" }) end,
    { desc = "[S]earch [N]otes" }, opts)

vim.keymap.set("n", "<leader>neo",
    function() builtin.git_files({ prompt_title = "Neovim Config", cwd = "~/.config/nvim" }) end,
    { desc = "[N]eovim [C]onfig" }, opts)

vim.keymap.set("n", "<leader>sf", builtin.find_files, { desc = "[S]earch [F]iles" }, opts)
vim.keymap.set("n", "<C-f>", builtin.find_files, { desc = "[S]earch [F]iles" }, opts)
vim.keymap.set("n", "<leader>sl", builtin.live_grep, { desc = "[S]earch [L]ive Grep" }, opts)

vim.keymap.set("n", "<leader>so", builtin.oldfiles, { desc = "[S]earch [O]ld Files" }, opts)
vim.keymap.set("n", "<leader>sw", builtin.grep_string, { desc = "[S]earch [W]ord" }, opts)

vim.keymap.set("n", "<leader>sg", builtin.git_files, { desc = "[S]earch [G]it Repository" }, opts)
vim.keymap.set("n", "<leader>gb", builtin.git_branches, { desc = "[G]it [B]ranch" }, opts)

vim.keymap.set("n", "<leader>sy", builtin.lsp_dynamic_workspace_symbols, { desc = "[S]earch s[Y]mbols" }, opts)

vim.keymap.set("v", "<leader>sv",
    function()
        local text = vim.getVisualSelection()
        builtin.live_grep({ default_text = text })
    end, { desc = "[S]earch [V]isual selection" }, opts)

vim.keymap.set("n", "<leader>su", "<cmd>Telescope undo<cr>", { desc = "[S]earch [U]ndo Tree" }, opts)

vim.keymap.set("n", "<leader>sb", builtin.current_buffer_fuzzy_find, { desc = "[S]earch inside [B]uffer" }, opts)
vim.keymap.set("n", "<leader>cb", builtin.current_buffer_fuzzy_find, { desc = "search [C]urrent [B]uffer" }, opts)
vim.keymap.set("n", "<C-b>", builtin.current_buffer_fuzzy_find, { desc = "[S]earch [C]urrent [B]uffer" }, opts)
vim.keymap.set("n", "<leader>bf", builtin.buffers, { desc = "[S]earch between [B]uffers" }, opts)
vim.keymap.set("n", "<leader>sr", builtin.registers, { desc = "[S]earch [R]egisters" }, opts)
vim.keymap.set("n", "<leader>sq", builtin.quickfix, { desc = "[S]earch [Q]uickfix" }, opts)
vim.keymap.set("n", "<leader>sp", builtin.pickers, { desc = "[S]earch [P]ichers history" }, opts)
vim.keymap.set("n", "<leader>sh", builtin.resume, { desc = "[S]earch [H]istory" }, opts)
vim.keymap.set("n", "<leader>st", builtin.builtin, { desc = "[S]earch [T]elescope Builtin" }, opts)
vim.keymap.set("n", "<leader>ht", builtin.help_tags, { desc = "[H]elp [T]ags" }, opts)
vim.keymap.set("n", "<leader>man", '<cmd>lua require("telescope.builtin").man_pages({sections={"ALL"}})<cr>',
    { desc = "[M]ove to [M]anual" }, opts)
vim.keymap.set("n", "<leader>cmd", builtin.commands, { desc = "[C]o[M]man[D]" }, opts)
vim.keymap.set("n", "<leader>sk", builtin.keymaps, { desc = "[S]earch [K]ey mapping" }, opts)


-- -- File Browser Extensions --
-- -- set_keymap('n', '<leader>nn', '<cmd>lua require("telescope").extensions.file_browser.file_browser({ cwd = vim.fn.expand("%:p:h") })<cr>', {desc='[N]avigate [N]nn'}, opts)
-- -- set_keymap('n', '<leader>n.', '<cmd>lua require("telescope").extensions.file_browser.file_browser({ cwd = "~/.config/dot_files/"})<cr>', {desc='[N]avigate [D]ot [F]iles'}, opts)
-- -- set_keymap('n', '<leader>ns', '<cmd>lua require("telescope").extensions.file_browser.file_browser({ cwd = "~/base"})<cr>', {desc='[N]avigate [S]cript'}, opts)

-- -- Debugger --
-- -- set_keymap("n", "<leader>dcc", '<cmd>lua require"telescope".extensions.dap.commands{}<cr>', opts)
-- -- set_keymap("n", "<leader>dco", '<cmd>lua require"telescope".extensions.dap.configurations{}<cr>', opts)
-- -- set_keymap("n", "<leader>dlb", '<cmd>lua require"telescope".extensions.dap.list_breakpoints{}<cr>', opts)
-- -- set_keymap("n", "<leader>dav", '<cmd>lua require"telescope".extensions.dap.variables{}<cr>', opts)
-- -- set_keymap("n", "<leader>daf", '<cmd>lua require"telescope".extensions.dap.frames{}<cr>', opts)


-- --[[ set_keymap('v', '??', 'y<cmd>lua builtin.current_buffer_fuzzy_find()<cr><C-r>"', opts) ]]
-- -- need refactor -- the idea was to visualy select and search in the current_buffer_fuzzy_find

-- --[[ set_keymap('n', '<leader>lc', '<cmd>lua builtin.loclist()<cr>', opts) ]]
-- --[[ set_keymap('n', '<leader>slc', '<cmd>lua builtin.loclist()<cr>', {desc='search Loclists'}, opts) ]]
-- --[[ set_keymap('n', '<leader>rsm', '<cmd>lua builtin.pickers()<cr>', opts) ]]
-- --[[ set_keymap('n', '<leader>pi', '<cmd>lua builtin.builtin()<cr>', opts) ]]

-- -- Git Files in specifique directory --
-- --[[ set_keymap('n', '<leader>dt', '<cmd>lua require("old.telescope_setvp").search_dotfiles()<cr>', opts) ]]
-- --[[ set_keymap('n', '<leader>sc', '<cmd>lua require("old.telescope_setvp").search_config()<cr>', opts) ]]
-- --[[ set_keymap('n', '<leader>nt', '<cmd>lua require("old.telescope_setvp").search_notes()<cr>', opts) ]]
