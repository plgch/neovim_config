-- Custom function attached to server
local nvim_lsp = require("lspconfig")

local custom_on_attach = function(client, bufnr)
    --require('completion').on_attach()

    -- Mappings.
    local opts = { buffer = bufnr, noremap = true, silent = true }

    if client.supports_method("textDocument/formatting") then
        vim.keymap.set("n", "<space>ft", function() vim.lsp.buf.format({ async = true }) end,
            { desc = "[F]orma[T] with Lsp" }, opts)
    end

    if client.server_capabilities.inlayHinterProvider then
        vim.keymap.set('n', '<leader>hh', function() vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled()) end,
            { desc = "toggle inlay [H]ints" }, { silent = true, noremap = true })
        vim.lsp.inlay_hint.enable(true, bufnr)
    end

    vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
    vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
    vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)
    vim.keymap.set("n", "<space>gD", vim.lsp.buf.type_definition, opts)
    vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
    vim.keymap.set("n", "<space>gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", { desc = "[G]oto [I]mplementation" },
        opts)
    vim.keymap.set("n", "<space>gd", "<cmd>lua require('telescope.builtin').lsp_definitions()<CR>",
        { desc = "[G]oto [D]efinition" }, opts)
    vim.keymap.set("n", "<space>gr", "<cmd>lua require('telescope.builtin').lsp_references()<CR>",
        { desc = "[G]oto [R]eference" }, opts)
    vim.keymap.set("n", "<space>rn", "<cmd>lua vim.lsp.buf.rename()<CR>", { desc = "[R]ename" }, opts)
    vim.keymap.set("n", "<space>ca", "<cmd>lua vim.lsp.buf.code_action()<CR>", { desc = "[C]ode [A]ction" }, opts)
    vim.keymap.set("n", "<space>hd", "<cmd>lua require('telescope.builtin').diagnostics()<CR>",
        { desc = "[H]elp [D]iagnostics" }, opts)
    vim.keymap.set("n", "<space>hf", "<cmd>lua vim.diagnostic.open_float()<CR>", { desc = "[H]elp [F]loat" }, opts)
    vim.keymap.set("n", "<space>gp", "<cmd>lua vim.diagnostic.goto_prev()<CR>",
        { desc = "[G]oto [P]revious [D]iagnostics" }, opts)
    vim.keymap.set("n", "<space>gn", "<cmd>lua vim.diagnostic.goto_next()<CR>", { desc = "[G]oto [N]ext [D]iagnostics" },
        opts)
end

local capabilities = require("cmp_nvim_lsp").default_capabilities()

-- Register / load / and attach server

-- List of server

-- local web_servers = {
--     'tsserver',
--     'html',
--     'cssls',
-- }

local servers = {
    "awk_ls",
    "clangd",
    -- "pyright",
    "pylsp",
    "jsonls",
    "marksman",
    "ruff",
    "lua_ls",
    'html',
    -- 'rust_analyzer',
}

for _, lsp in ipairs(servers) do
    nvim_lsp[lsp].setup({
        capabilities = capabilities,
        on_attach = custom_on_attach,
    })
end

--[[ -- Special Spec for html -- ]]
--[[ nvim_lsp['html'].setup { ]]
--[[     capabilities = capabilities, ]]
--[[     on_attach = custom_on_attach ]]
--[[ } ]]

-- Config for diagnostic display features
vim.diagnostic.config({
    virtual_text = false,
    -- virtual_text = {
    -- 	source = "if_many",
    -- 	spacing = 2,
    -- 	prefix = "",
    -- },
    float = {
        source = "if_many",
    },
    severity_sort = true,
    update_in_insert = true,
    signs = true,
    underline = true,
})

-- BASH / ZSH --
require("lspconfig").bashls.setup({
    capabilities = capabilities,
    on_attach = custom_on_attach,
    filetypes = { "sh", "bash", "zsh" },
})

-- LUA --
-- Make runtime files discoverable to the server
-- local runtime_path = vim.split(package.path, ';')
-- table.insert(runtime_path, 'lua/?.lu')
-- table.insert(runtime_path, 'lua/?/init.lua')
--
-- require('lspconfig').lua_ls.setup {
--     on_attach = custom_on_attach,
--     capabilities = capabilities,
--     settings = {
--         Lua = {
--             runtime = {
--                 -- Tell the language server which version of Lua you're using (most likely LuaJIT)
--                 version = 'LuaJIT',
--                 -- Setup your lua path
--                 path = runtime_path,
--             },
--             diagnostics = {
--                 globals = { 'vim' },
--             },
--             --[[ workspace = { library = vim.api.nvim_get_runtime_file('', true) }, ]]
--             -- Do not send telemetry data containing a randomized but unique identifier
--             checkThirdParty = false,
--             telemetry = { enable = false },
--         },
--     },
-- }
