vim.o.completeopt = "menuone,noselect"

local cmp = require("cmp")

vim.api.nvim_set_hl(0, "MyNormal", { bg = "#282828" })
vim.api.nvim_set_hl(0, "MyFloatNormal", { bg = "#282828" })
-- vim.api.nvim_set_hl(0, "CmpItemAbbrMatchFuzzy", { fg = "#d3869b", bg = "NONE"})
-- vim.api.nvim_set_hl(0, "CmpItemAbbrMatch", { fg = "#d3869b", bg = "NONE"})
-- vim.api.nvim_set_hl(0, "CmpItemAbbr", { fg = "#d3869b", bg = "NONE"})

cmp.setup({
    snippet = {
        -- REQUIRED - you must specify a snippet engine
        expand = function(args)
            require("luasnip").lsp_expand(args.body) -- For `luasnip` users.
        end,
    },

    formatting = {
        format = function(entry, vim_item)
            -- set a name for each source
            vim_item.menu = ({
                buffer = "[Buffer]",
                nvim_lsp = "[LSP]",
                treesitter = "[Treesitter]",
                path = "[Path]",
            })[entry.source.name]
            return vim_item
        end,
    },

    window = {
        -- completion = cmp.config.window.bordered({
        completion = {
            -- winhighlight = "Normal:MyNormal,FloatBorder:MyFloatNormal",
            --     col_offset = -1,
            -- line_offset = -1,
        },

        -- documentation = cmp.config.window.bordered({
        documentation = {
            winhighlight = "Normal:MyNormal,FloatBorder:MyFloatNormal",
            -- col_offset = -1,
        },
    },

    mapping = cmp.mapping.preset.insert({
        ["<C-d>"] = cmp.mapping.scroll_docs(8),
        ["<C-u>"] = cmp.mapping.scroll_docs(-8),
        -- ['<C-f>'] = cmp.mapping.scroll_docs(4),
        -- ['<C-Space>'] = cmp.mapping.complete(),
        ["<C-e>"] = cmp.mapping.close(),
        ["<CR>"] = cmp.mapping.confirm({ select = true }),
        -- ['<C-n'] = cmp.mapping.select_next_item(),
        -- ['<C-p'] = cmp.mapping.select_prev_item(),
    }),

    sources = {
        { name = "nvim_lsp",   max_item_count = 50 },
        { name = "treesitter", max_item_count = 20 },
        -- For luasnip user.
        { name = "luasnip",    max_item_count = 20 },

        { name = "path",       max_item_count = 20 },
        { name = "buffer",     max_item_count = 20 },
    },
})

-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline("/", {
    sources = {
        { name = "buffer", max_item_count = 20 },
    },
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(":", {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
        { name = "path", max_item_count = 20 },
    }, {
        { name = "cmdline", max_item_count = 20 },
    }),
})
