-- if I want to overrides someday
-- local palette = require("gruvbox.palette")
--
-- local border = {
--     bg = palette.dark0_soft,
--     fg = palette.gray
-- }
--
-- local overrides = {
--     TelescopeResultsBorder = border,
--     TelescopeBorder = border,
--     TelescopePreviewBorder = border,
--     TelescopePromptBorder = border,
-- }

-- setup must be called before loading the colorscheme
-- Default options:

return {
    {
        "ellisonleao/gruvbox.nvim",
        lazy = false,
        priority = 1000,
        config = function()
            -- we can either make specifique file for the configuration with
            -- require "custom.telescope"
            -- or:
            require("gruvbox").setup({
                undercurl = true,
                underline = true,
                bold = false,
                italic = {
                    strings = false,
                    comments = false,
                    operators = false,
                    folds = false,
                },
                strikethrough = true,
                invert_selection = true,
                invert_signs = false,
                invert_tabline = false,
                invert_intend_guides = false,
                inverse = true,    -- invert background for search, diffs, statuslines and errors
                contrast = "soft", -- can be "hard", "soft" or empty string
                -- overrides = overrides,
            })

            vim.cmd("colorscheme gruvbox")
            -- I also like this one but its not well supported
            -- vim.cmd("colorscheme habamax")
        end,
    },
}
