return {
    "mfussenegger/nvim-lint",
    dependencies = {
        "williamboman/mason.nvim",
        "rshkarin/mason-nvim-lint",

    },
    config = function()
        require("mason").setup()
        require("mason-nvim-lint").setup({
            automatic_installation = true,
        })
        local lint = require("lint")

        lint.linters_by_ft = {
            python = {
                -- "flake8",
                -- "mypy",
                -- "pylint",
            },
            lua = {
                -- needs luarocks / package manager of lua
                -- "luacheck",
            },
            markdown = {
                -- needs initialization
                -- 'vale',
            },
            shell = {
                "shellcheck"
            },
        }

        local lint_augroup = vim.api.nvim_create_augroup("lint", { clear = true })

        vim.api.nvim_create_autocmd({ "BufWritePost" }, {
            group = lint_augroup,
            callback = function()
                lint.try_lint()
            end,
        })

        vim.keymap.set("n", "<leader>li", function()
            lint.try_lint()
        end, { desc = "Trigger linting for current file" })
    end
}
