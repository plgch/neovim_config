return {
    "anuvyklack/windows.nvim",
    dependencies = {
        "anuvyklack/middleclass"
    },
    config = function()
        require("windows").setup({
            autowidth = {
                enable = true,
                winwidth = 5,
                filetype = {
                    help = 2,
                },
            },
            ignore = {
                buftype = { "quickfix" },
                filetype = { "NvimTree", "neo-tree", "undotree", "NeogitStatus", "toggleterm" },
            },
            animation = {
                enable = false,
            },
        })
    end,
    vim.keymap.set("n", "<leader>mm", "<cmd>WindowsMaximize<CR>", { desc = "[M]aximize" }, opts)
}
