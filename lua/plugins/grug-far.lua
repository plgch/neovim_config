return {
    'MagicDuck/grug-far.nvim',
    config = function()
        vim.g.maplocalleader = ','
        require('grug-far').setup({
            -- ... options, see Configuration section below ...
            -- ... there are no required options atm...
            windowCreationCommand = 'topleft vsplit',
        });
        vim.keymap.set("n", "<leader>rg", "<cmd>GrugFar<cr>", { desc = "[R]ip [G]rugFar" }, opts)
        vim.keymap.set("n", "<leader>rw", "<cmd>lua require('grug-far').grug_far({ prefills = { search = vim.fn.expand('<cword>') } })<cr>", { desc = "[R]ip GrugFar [W]ord" }, opts)
    end
}
