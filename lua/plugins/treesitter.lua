return {
    -- TreeSitter --
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        dependencies = {
            "nvim-treesitter/nvim-treesitter-refactor",
            -- "nvim-treesitter/playground",
            -- deprecated use :InspectTree
        },
        config = function()
            require("nvim-treesitter.configs").setup({
                ensure_installed = {
                    "c",
                    "lua",
                    "bash",
                    "python",
                    "awk",
                    "html",
                    "css",
                    "javascript",
                    "typescript",
                    "regex",
                    "json",
                    "rust",
                    "toml",
                    "yaml",
                    "markdown",
                },
                highlight = {
                    enable = true,
                    --disable = {"lua"},
                    disable = { "markdown" },
                },
                indent = {
                    enable = true,
                },
                incremental_selection = {
                    enable = true,
                    keymaps = {
                        -- init_selection = "<c-space>",
                        --[[ node_incremental = "<leader>k", ]]
                        --[[ scope_incremental = "<leader>h", ]]
                        -- node_incremental = "<c-space>",
                        scope_incremental = "<c-s>",
                        -- node_decremental = "<leader>j",
                        init_selection = "<Enter>",
                        node_incremental = "<Enter>",
                        node_decremental = "<BS>",
                    },
                },
                -- deprecated use require('nvim-ts-autotag')
                -- autotag = {
                --     enable = true,
                -- },
                refactor = {
                    smart_rename = {
                        enable = true,
                        keymaps = {
                            smart_rename = "rn",
                        },
                    },
                },
            })
        end,
    },
    {
        "windwp/nvim-ts-autotag",
        config = function()
            require('nvim-ts-autotag').setup({})
        end,
    },
}
