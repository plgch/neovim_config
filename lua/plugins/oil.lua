return {
    "stevearc/oil.nvim",
    opts = {},
    -- Optional dependencies
    -- dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
        local oil = require("oil")
        oil.setup({
            -- Id is automatically added at the beginning, and name at the end
            -- See :help oil-columns

            -- disable default keymaps
            use_default_keymaps = false,

            delete_to_trash = true,

            skip_confirm_for_simple_edits = true,

            watch_for_changes = true,

            -- setting our own keymaps
            keymaps = {
                -- ["<C-v>"] = "actions.select_vsplit",
                -- ["<C-h>"] = "actions.select_split",
                ["g?"] = "actions.show_help",
                ["<CR>"] = "actions.select",
                ["<C-c>"] = "actions.close",
                ["<C-r>"] = "actions.refresh",
                ["<C-t>"] = "actions.select_tab",
                ["<C-p>"] = "actions.preview",
                ['<C-u>'] = 'actions.preview_scroll_up',
                ['<C-d>'] = 'actions.preview_scroll_down',
                [".."] = "actions.parent",
                ["cd"] = "actions.cd",
            },

            columns = {
                -- "icon",
                "permissions",
                "size",
                "mtime",
            },

            view_options = {
                -- Show files and directories that start with "."
                show_hidden = true,
                -- This function defines what is considered a "hidden" file
                is_hidden_file = function(name, bufnr)
                    return vim.startswith(name, ".")
                end,
                -- This function defines what will never be shown, even when `show_hidden` is set
                is_always_hidden = function(name, bufnr)
                    return false
                end,
            },
        })

        vim.keymap.set('n', '<leader>nn', function()
            oil.open()

            -- Wait until oil has opened, for a maximum of 1 second.
            vim.wait(1000, function()
                return oil.get_cursor_entry() ~= nil
            end)
            if oil.get_cursor_entry() then
                oil.open_preview()
            end
        end, { desc = "[N]avigate" })
        vim.keymap.set("n", "<leader>la", oil.open, { desc = "[L]ist [A]ll file/directory" })
        vim.keymap.set("n", "<leader>nd", function()
            oil.open("~/.config/dot_files/")
        end, { desc = "[N]avigate [D]otfiles" })
        vim.keymap.set("n", "<leader>nc", function()
            oil.open("~/.config/nvim/")
        end, { desc = "[N]avigate [C]onfig nvim" })
        vim.keymap.set("n", "<leader>nb", function()
            oil.open("~/notes/")
        end, { desc = "[N]avigate [B]rain" })
        vim.keymap.set("n", "<leader>ns", function()
            oil.open("~/base/")
        end, { desc = "[N]avigate [S]cript" })
    end,
}
