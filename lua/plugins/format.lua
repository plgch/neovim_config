return {
    -- Format --
    -- {
    --     "stevearc/conform.nvim",
    --     config = function()
    --         local conform = require("conform")
    --         conform.setup({
    --             formatters_by_ft = {
    --                 lua = { "stylua" },
    --                 python = {
    --                     -- "black",
    --                     -- "isort",
    --                 },
    --             },
    --             -- format_on_save = {
    --             -- 	lsp_fallback = true,
    --             -- 	async = false,
    --             -- 	timeout_ms = 1000,
    --             -- },
    --         })
    --         vim.keymap.set({ "n", "v" }, "<leader>fc", "<cmd>Format<cr>", { desc = "[F]ormat with [C]onform.nvim" })
    --         vim.api.nvim_create_user_command("Format", function()
    --             require("conform").format({
    --                 lsp_fallback = true,
    --                 async = false,
    --                 timeout_ms = 1000,
    --             })
    --         end, { desc = "[F]ormat" })
    --     end,
    -- },
    -- Indent --
    {
        "lukas-reineke/indent-blankline.nvim",
        main = "ibl",
        opts = {},
        config = function()
            require("ibl").setup({
                exclude = {
                    buftypes = { "terminal" },
                    -- filetypes = { "c" },
                },
                indent = {
                    -- space_char_blankline = " ",
                    -- smart_indent = true,
                    tab_char = ".",
                    char = "|",
                },
                whitespace = {
                    -- space_char_blankline = " ",
                    -- exclude = { language = { "c" } },
                },
                scope = {
                    enabled = true,
                    show_start = false,
                    show_end = false,
                    -- show_current_context = true,
                    -- show_current_context_start = true,
                    -- space_char_blankline = " ",
                    exclude = { language = { "c" } },
                },
            })
            -- Snippet --
            vim.keymap.set({ "i", "s" }, "<C-l>", '<cmd>lua require("luasnip").jump(1)<CR>',
                { desc = "[N]avigate Snippet (1)" }, opts)
            vim.keymap.set({ "i", "s" }, "<C-h>", '<cmd>lua require("luasnip").jump(-1)<CR>',
                { desc = "[N]avigate Snippet (-1)" }, opts)
        end,
    },
}
