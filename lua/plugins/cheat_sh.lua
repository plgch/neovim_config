return {
    {
        "RishabhRD/nvim-cheat.sh",
        dependencies = {
            "RishabhRD/popfix",
        },
        config = function()
            vim.g.cheat_default_window_layout = "vertical_split"
            vim.keymap.set("n", "<leader>cht", "<cmd>Cheat<cr>", { desc = "open cheat query" }, opts)
        end,
    },
}
