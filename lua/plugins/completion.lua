return {
    -- nvim cmp --
    {
        "hrsh7th/nvim-cmp",

        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-cmdline",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "ray-x/cmp-treesitter",
        },
        config = function()
            require("configuration.completion")
        end,
    },
    -- snippet --
    {
        "L3MON4D3/LuaSnip",
        dependencies = {
            "hrsh7th/nvim-cmp",
        },
        -- follow latest release.
        version = "v2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
        -- install jsregexp (optional!).
        build = "make install_jsregexp",
        config = function()
            require("cmp").setup({
                snippet = {
                    -- REQUIRED - you must specify a snippet engine
                    expand = function(args)
                        require("luasnip").lsp_expand(args.body) -- For `luasnip` users.
                    end,
                },
            })
        end,
    },
    -- autopairs --
    {
        "windwp/nvim-autopairs",
        dependencies = {
            "hrsh7th/nvim-cmp",
        },

        config = function()
            require("nvim-autopairs").setup({})

            -- you need setup cmp first put this after cmp.setup()
            require("cmp").setup({
                map_cr = true,        --  map <CR> on insert mode
                map_complete = false, -- it will auto insert `(` (map_char) after select function or method item
                auto_select = true,   -- automatically select the first item
                insert = false,       -- use insert confirm behavior instead of replace
                map_char = {          -- modifies the function or method delimiter by filetypes
                    all = { "[", "(", "{" },
                },
            })
        end,
    },
}
