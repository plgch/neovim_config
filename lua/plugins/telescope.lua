return {
    "nvim-telescope/telescope.nvim",
    dependencies = {
        "nvim-lua/popup.nvim",
        "nvim-lua/plenary.nvim",
        "debugloop/telescope-undo.nvim",
        -- "nvim-telescope/telescope-fzy-native.nvim",
        "nvim-telescope/telescope-ui-select.nvim",
        -- "nvim-telescope/telescope-media-files.nvim",
        -- "nvim-telescope/telescope-frecency.nvim"
        -- "nvim-telescope/telescope-smart-history.nvim",
        -- "nvim-telescope/telescope-file-browser.nvim",
        -- "nvim-telescope/telescope-dap.nvim",
        { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
    },
    config = function()
        require("configuration.telescope")
    end,
}
