return {
    -- Signs --
    {
        "lewis6991/gitsigns.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        config = function()
            require("gitsigns").setup({
                signs = {
                    add = { text = "+" },
                },
            })
        end,
    },
    -- Neogit --
    {
        "NeogitOrg/neogit",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "sindrets/diffview.nvim",
            "nvim-telescope/telescope.nvim",
        },
        config = function()
            local neogit = require("neogit")
            local opts = { noremap = true, silent = true }

            neogit.setup({
                disable_signs = false,
                disable_context_highlighting = false,
                disable_commit_confirmation = false,

                -- customize displayed signs
                signs = {
                    section = { ">", "v" },
                    item = { ">", "v" },
                    hunk = { "", "" },
                },

                integrations = {
                    telescope = true,
                    diffview = true,
                },
            })
        end,
        vim.keymap.set("n", "<leader>gs", '<cmd> lua require("neogit").open({ cwd = vim.fn.expand("%:p:h") })<cr>',
            { desc = "[G]it [S]tatus" }, opts),
    },
}
