return {
    -- Status Line --
    {
        "nvim-lualine/lualine.nvim",
        dependencies = { "nvim-tree/nvim-web-devicons" },
        config = function()
            require("lualine").setup({
                options = {
                    theme = "auto",
                    component_separators = { left = "|", right = "|" },
                    section_separators = { left = "", right = "" },
                    icons_enabled = false,
                    globalstatus = true,
                },
                sections = {
                    lualine_a = { { "mode", upper = true } },
                    lualine_b = { "branch", "diff" },
                    lualine_c = { "diagnostics", { "filename", file_status = true } },
                    lualine_x = { "encoding", "fileformat", "filetype" },
                    lualine_y = { "progress" },
                    lualine_z = { "location" },
                },
                inactive_sections = {
                    lualine_a = {},
                    lualine_b = {},
                    lualine_c = { "filename" },
                    lualine_x = { "location" },
                    lualine_y = {},
                    lualine_z = {},
                },
                extensions = { "man", "fzf", "lazy", "mason", "oil", "toggleterm" },
            })
        end,
    },
    -- Rainbow Parentheses --
    {
        "HiPhish/rainbow-delimiters.nvim",
        config = function()
            require("rainbow-delimiters.setup").setup({
                strategy = {},
                query = {},
                highlight = {},
            })
        end,
    },
    -- Colorize this: #ff9900 --
    {
        "norcalli/nvim-colorizer.lua",
        config = function()
            require("colorizer").setup()
        end,
    },
    -- CSV Colors
    {
        "mechatroner/rainbow_csv",
    },
    -- CSV viewer --
    {
        'hat0uma/csvview.nvim',
        config = function()
            require('csvview').setup()
        end
    },
    -- Notification System --
    {
        "rcarriga/nvim-notify",
        config = function()
            local notify = require("notify")

            notify.setup({
                stages = "fade_in_slide_out",
                background_colour = "FloatShadow", -- same as "#000000",
                timeout = 4200,
            })
            vim.notify = notify
        end,
    },
}
