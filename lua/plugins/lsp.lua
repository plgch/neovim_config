return {
    {
        "neovim/nvim-lspconfig",
        dependencies = {
            -- Mason
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",
            -- UI for LSP --
            {
                "j-hui/fidget.nvim",
                tag = "legacy",
                event = "LspAttach",
            },
        },
        config = function()
            require("mason").setup()

            require("mason-lspconfig").setup({
                automatic_installation = true,
            })
            require("fidget").setup()
            require("configuration.lsp")
        end,
    },
}
