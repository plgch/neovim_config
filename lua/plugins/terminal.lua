return {
    -- ToggleTerm --
    "akinsho/toggleterm.nvim",
    version = "*",
    config = function()
        require("toggleterm").setup({
            -- open_mapping = [[<leader>tt]],
            start_in_insert = true,
            persist_size = true,
            persist_mode = true,   -- if set to true (default) the previous terminal mode will be remembered
            direction = "float",
            close_on_exit = false, -- close the terminal window when the process exits
            auto_scroll = true,    -- automatically scroll to the bottom on terminal output
            hide_numbers = false,
            shade_terminals = true,
            shading_factor = 1,
            float_opts = {
                -- The border key is *almost* the same as 'nvim_open_win'
                -- see :h nvim_open_win for details on borders however
                -- the 'curved' border is a custom border type
                -- not natively supported but implemented in this plugin.
                -- [[ border = 'single' | 'double' | 'shadow' | 'curved' |, ]]
                -- like `size`, width and height can be a number or function which is passed the current terminal
                border = "single",
                width = vim.o.columns - 8,
                height = vim.o.lines - 8,
            },
            highlights = {
                -- highlights which map to a highlight group name and a table of it's values
                -- NOTE: this is only a subset of values, any group placed here will be set for the terminal window split
                Normal = {
                    guibg = "#32302f",
                },
                FloatBorder = {
                    guifg = "#504945",
                    -- guibg = "<VALUE-HERE>",
                },
            },
            winbar = {
                enabled = false,
                name_formatter = function(term) --  term: Terminal
                    return term.name
                end,
            },
        })
        vim.keymap.set("n", "<leader>tf", "<cmd>ToggleTerm direction=float<cr>", { desc = "[T]erminal [F]loat" }, opts)
        vim.keymap.set("n", "<leader>tv", "<cmd>ToggleTerm size=80 direction=vertical<cr>",
            { desc = "[T]erminal [V]ertical" }, opts)
        vim.keymap.set("n", "<leader>th", "<cmd>ToggleTerm size=10 direction=horizontal<cr>",
            { desc = "[T]erminal [H]orizontal" }, opts)
        vim.keymap.set("n", "<leader>tt", "<cmd>ToggleTerm<cr>", { desc = "[T]erminal [T]oggle" }, opts)
        vim.keymap.set("n", "<leader>tc", "<cmd>ToggleTermSendCurrentLine<cr>",
            { desc = "[T]erminal Send [C]urrent Line" }, opts)
        vim.keymap.set("n", "<leader>to", "<cmd>ToggleTermSendCurrentLine<cr><cmd>ToggleTerm<cr>",
            { desc = "[T]erminal Send & [O]pen" }, opts)
        vim.keymap.set("v", "<leader>tc", "<cmd>ToggleTermSendVisualSelection<cr>",
            { desc = "[T]erminal Send [Current] Selection" }, opts)
        vim.keymap.set("v", "<leader>to", "<cmd>ToggleTermSendVisualSelection<cr><cmd>ToggleTerm<cr>",
            { desc = "[T]erminal Send Selection & [O]pen" }, opts)
        -- vim.keymap.set('v', '<leader>tv',    '<cmd>ToggleTermSendVisualLines<cr>', opts)
    end,
}

-- -- Calling require 'term-edit'.setup(opts) is mandatory
-- require("term-edit").setup {
--     -- Mandatory option:
--     -- Set this to a lua pattern that would match the end of your prompt.
--     -- Or a table of multiple lua patterns where at least one would match the
--     -- end of your prompt at any given time.
--     -- For most bash/zsh user this is '%$ '.
--     -- For most powershell/fish user this is '> '.
--     -- For most windows cmd user this is '>'.
--     prompt_end = '%$ ',
--     -- How to write lua patterns: https://www.lua.org/pil/20.2.html
-- }
