-- Need to make a good strucure for keybind
-- leader c : commands / runners
-- leader d : debugger / breakpoints
-- leader b : buffer
-- leader m : movement / maximize / move to
-- leader n : nnn / tree
-- Leader t : terminal
-- Leader f : format
-- Leader s : search
-- Leader v : version
-- Leader g : git
-- Leader r : refactoring
-- Leader h : help / diagnostique / hover

return {
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
        opts = {
            delay = 200,
            icons = {
                rules = false,
            },
        },
    },
}
